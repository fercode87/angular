import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {UserService} from '../../services/user.service';
import {CategoryService} from '../../services/category.service';
import {PostService} from '../../services/post.service'
import {Post} from '../../models/post';
import {global} from'../../services/global';

@Component({
  selector: 'app-post-edit',
  templateUrl: '../post-new/post-new.component.html',
  styleUrls: ['../post-new/post-new.component.css'],
  providers: [UserService, CategoryService, PostService]
})
export class PostEditComponent implements OnInit {
public page_title: string;
public token;
public identity;
public post: Post;
public url:string;
public categories;
public status;
public is_edit: boolean;
public froala_options: Object = {
  charCounterCount: true,
  language: 'es',
  toolbarButtons: ['bold', 'italic', 'underline', 'paragraphFormat'],
  toolbarButtonsXS: ['bold', 'italic', 'underline', 'paragraphFormat'],
  toolbarButtonsSM: ['bold', 'italic', 'underline', 'paragraphFormat'],
  toolbarButtonsMD: ['bold', 'italic', 'underline', 'paragraphFormat'],
};
public afuConfig = {
  multiple: false,
  formatsAllowed: ".jpg,.png , .gif , .jpeg",
  maxSize: "50",
  uploadAPI:  {
    url: global.url+"post/upload",
    headers: {   
   "Authorization" : this._userService.getToken()
    }
  },
  theme: "attachPin",
  hideProgressBar: false,
  hideResetBtn: true,
  hideSelectBtn: true,
  attachPinText: 'Sube la imagen de este articulo',
    };



  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _categoryService: CategoryService,
    private _postService: PostService

  ) { 
    this.page_title ='Editar Entrada';
    this.identity = _userService.getIdentity();
    this.token = _userService.getToken();
    this.is_edit=true;
    this.url=global.url;
  }

  ngOnInit(){
    this.getCategories();
    this.getPost();
    this.post = new Post (1,this.identity.sub,1,'','',null,null);
    
  }
  
  getCategories(){
    this._categoryService.getCategories().subscribe(
      response=>{
        if(response.status=='success' ){
          this.categories=response.categories;
          console.log(this.categories);

        }

      },error=>{
        console.log(error);

      }
    )
  }
  imageUpload(data){
    let image_data = JSON.parse(data.response) ;
    this.post.image= image_data.image;
  }
  getPost(){
    this._route.params.subscribe(params=>{
      let id = +params['id'];
      this._postService.getPost(id).subscribe(
              response=> {
              if(response.status=='success'){
                this.post=response.post;
                if(this.post.user_id != this.identity.sub){
                this._router.navigate(['/inicio']);

                }  
              }
              },error=>{
                console.log(error);
                this._router.navigate(['/inicio']);
              }
                  );
                });
  }
  
  
  onSubmit(form){
    this._postService.update(this.token, this.post, this.post.id).subscribe(
      response=>{
                  if(response.status=='success'){
                    this.status='success';
                    this._router.navigate(['/inicio']);

                  }else{
                    this.status='error';
                  }
                        },error=>{
                          this.status='error';
                          console.log(error);
                        }
    );
}
}
